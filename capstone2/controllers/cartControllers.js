const Carts = require("../models/Carts.js");
const Products = require("../models/Products.js");

const auth = require("../auth.js");

module.exports.createCart = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	if(userData.isAdmin){

		return response.send(false);

	} else {

		Carts.findOne({userId: userData.id})
		.then(find => {
			if(find){

				return response.send(false);

			} else {

				let newCart = new Carts({

					userId: userData.id,
					totalAmount: 0


				})

				newCart.save()
				.then(saved => response.send(true))
				.catch(error => response.send(false));
			}
		})
	}
}



module.exports.addToCart = async (request, response) => {

	const userData = auth.decode(request.headers.authorization)

	const product = request.body;




	if(userData.isAdmin){

		return response.send(false);

	} else {

		let subtotal = 0;
		let productName = '';

		let cart =  await Carts.findOne({userId: userData.id})
		.then(result => {

			return result

		})
		.catch(error => response.send(error))

		await Products.findById(product.productId)
		.then(result => {

			subtotal = result.price * product.quantity;

			productName = result.name;

		})

		let isExisting = false;

		for( let i in cart.products){

			let cartProduct = cart.products[i];
			if(cartProduct.productId == product.productId){

				cartProduct.quantity = cartProduct.quantity + product.quantity;

				cartProduct.subtotal = cartProduct.subtotal + subtotal;

				isExisting = true;

			} 
		}
		if(!isExisting){
			cart.products.push({
				productName: productName,
				productId: product.productId,
				quantity: product.quantity,
				subtotal: subtotal
			})
		}

		cart.totalAmount = cart.totalAmount + subtotal;

		let isCartUpdated = await cart.save()
		.then(saved => true)
		.catch(error => response.send(error))


		if(isCartUpdated){
			return response.send(true)
		} else {
			return response.send(false)
		}
	}
}

module.exports.updateQuantity = async (request, response) => {

	const userData = auth.decode(request.headers.authorization)

	const product = request.body;


	if(userData.isAdmin){

		return response.send("Admin is not allowed to Order!");

	} else {

		let subtotal = 0;

		let cart = await Carts.findOne({userId: userData.id})
		.then(result => {

			return result

		})
		.catch(error => response.send(error))

		await Products.findById(product.productId)
		.then(result => {

			subtotal = result.price * product.quantity;

		})

		let oldSubtotal = 0;

		for( let i in cart.products){

			let cartProduct = cart.products[i];
			if(cartProduct.productId == product.productId){

				cartProduct.quantity = product.quantity;
				oldSubtotal = cartProduct.subtotal;
				cartProduct.subtotal = subtotal;

			} 
		}

		cart.totalAmount = cart.totalAmount - oldSubtotal + subtotal;

		let isCartUpdated = await cart.save()
		.then(saved => true)
		.catch(error => response.send(error))


		if(isCartUpdated){
			return response.send(`Update Quantity Successful!`)
		} else {
			return response.send(`There was an error in updating the item please try again!`)
		}
	}
}


module.exports.deleteProduct = async (request, response) => {

	const userData = auth.decode(request.headers.authorization)

	const product = request.body;


	if(userData.isAdmin){

		return response.send("Admin is not allowed to Order!");

	} else {

		let cart = await Carts.findOne({userId: userData.id})
		.then(result => {

			return result

		})
		.catch(error => response.send(error))

		let productIndex = null;
		let newSubtotal = 0;

		for( let i in cart.products){


			let cartProduct = cart.products[i];
			if(cartProduct.productId == product.productId){

				productIndex = i;

				break;
			} 

		} 
		if(productIndex == null){

			 return response.send(`Product cannot be found!`);
			 
		}


		let removeProduct = cart.products[productIndex];

		// array.splice(index, 1)

		cart.totalAmount =  cart.totalAmount - removeProduct.subtotal;

		cart.products.splice(productIndex, 1)

		let isCartUpdated = await cart.save()
		.then(saved => true)
		.catch(error => response.send(error))

		if(isCartUpdated){
			return response.send(`Delete Successful!`)
		} else {
			return response.send(`There was an error in updating the item please try again!`)
		}
	}

}



module.exports.viewCart = (request, response) => {

	const userData = auth.decode(request.headers.authorization)

	Carts.findOne({userId: userData.id})
	.then(data => response.send(data))
	.catch(error => response.send(false))

} 