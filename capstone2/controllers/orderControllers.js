const Orders = require("../models/Orders.js")

const Products = require("../models/Products.js");

const auth = require("../auth.js")



module.exports.singleOrder = async (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	const products = request.body;

	if(userData.isAdmin){

		return response.send("Admin is not allowed to Order!");

	} else {

		let totalAmount = 0;

		await Products.findById(products.productId)
		.then(result => {

			totalAmount = result.price * products.quantity;

		})

		let order = new Orders({

			userId: userData.id,
			products,
			totalAmount
		})

		order.save()
		.then(saved => response.send(`Orders complete \n${saved}`))
		.catch(error => response.send(error));

	}
}


module.exports.multipleOrders = async (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	if(userData.isAdmin){

		return response.send("Admin is not allowed to Order!");

	} else {

		let products = request.body;
		let totalAmount = 0;

		for(let i in products){

			let product = products[i];
			let amount = 0;

			await Products.findById(product.productId)
			.then(result => {

				amount = result.price * product.quantity;
				totalAmount = totalAmount + amount

			})	
		}

		let order = new Orders({

			userId: userData.id,
			products,
			totalAmount

		});

		order.save()
		.then(save => response.send(save))
		.catch(error => response.send(error));

	}

}

module.exports.findAll = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	if(userData.isAdmin){

		Orders.find({})
		.then(result => response.send(result))
		.catch(error => response.send(error));
	} else {

		return response.send(`You Don't have access	in this Route!`);
		
	}
}