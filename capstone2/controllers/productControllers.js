const Products = require("../models/Products.js");

const auth = require("../auth.js");

module.exports.addProducts = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	if(!userData.isAdmin){

		return response.send(false)

	} else {

		let newProduct = new Products({
			name: request.body.name,
			description: request.body.description,
			price: request.body.price,
			stocks: request.body.stocks
		})

		newProduct.save()
		.then(save => response.send(true))
		.catch(error => response.send(false));
	}
}

module.exports.allProducts = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	if(userData.isAdmin){
		Products.find({})
		.then(result => response.send(result))
		.catch(error => response.send(error));
	} else {

		return response.send(`You are Not an Admin!`)
	}

}

module.exports.activeProducts = (request, response) => {

	Products.find({isActive: true})
	.then(result => response.send(result))
	.catch(error => response.send(error));
}

module.exports.specificProduct = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	const productId = request.params.productId;

	if(userData.isAdmin){
		Products.findById(productId)
		.then(result => response.send(result))
		.catch(error => response.send(error));

	} else {

		return response.send(`You are Not an Admin!`)

	}
}

module.exports.updateProduct = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	const productId = request.params.productId;
	let isActive = true

	let updatedProduct = {
		name: request.body.name,
		description: request.body.description,
		price:request.body.price,
		stocks: request.body.stocks,
		isActive: request.body.isActive
	}

	if(userData.isAdmin){

		Products.findByIdAndUpdate(productId, updatedProduct)
		.then(result => response.send(true))
		.catch(error => response.send(false));

	} else {

		return response.send(false);
	}
}

module.exports.archiveProduct = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	const productId = request.params.productId;

	let archiveProduct = {
		isActive: request.body.isActive
	}

	if(userData.isAdmin){

		Products.findByIdAndUpdate(productId, archiveProduct)
		.then(result => {
			if(result.isActive == false){
				return response.send(true);
			} else {
				return response.send(true);
			}
		})
		.catch(error => response.send(error));
	} else {

		return response.send(false)
	}
}

module.exports.viewProduct = (request, response) => {

	const productId = request.params.productId;
	Products.findById(productId)
	.then(result => response.send(result))
	.catch(error => response.send(false)); 
}