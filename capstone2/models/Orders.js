const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	userId: {
		type: String,
		require: [true, "User Id is required!"]
	},
	products: [
		{
			productId: {
				type: String,
				require: [true, "Ordered object Id is required!" ]
			},
			quantity: {
				type: Number,
				require: [true, "Ordered quantity is required!"]
			}
		}
	],
	totalAmount: {
		type: Number,
		require: [true, "Ordered total Amount is required!"]
	},
	purchasedOn: {
		type: Date,
		default: new Date()
	}
})

const Orders = new mongoose.model("Order", orderSchema);

module.exports = Orders;