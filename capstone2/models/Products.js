const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	name: {
		type: String,
		require: [true, "Product Name is required!"]
	},
	description: {
		type: String,
		require: [true, "Product description is required!"]
	},
	price: {
		type: Number,
		require: [true, "Product price is required!"]
	},
	stocks: {
		type: Number,
		require: [true, "Product stocks is required!"]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date
	},
})
	
const Products = mongoose.model("Products", productSchema);

module.exports = Products;


