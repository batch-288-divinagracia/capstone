const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	email: {
		type: String,
		require: [true, "User email is required!"]
	},
	password: {
		type: String,
		require: [true, "User password is required!"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
})

const Users = mongoose.model("User", userSchema);

module.exports = Users;


