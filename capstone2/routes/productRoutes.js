const express = require("express");

const productControllers = require("../controllers/productControllers.js");

const auth = require("../auth.js")

const router = express.Router();

// routers

router.post("/addProducts", auth.verify, productControllers.addProducts);

router.get("/findAll", auth.verify, productControllers.allProducts);

router.get("/availableProducts", productControllers.activeProducts);

router.get("/:productId", auth.verify, productControllers.specificProduct);

router.get("/:productId/view", productControllers.viewProduct);

router.patch("/:productId", auth.verify, productControllers.updateProduct);

router.patch("/:productId/archive", auth.verify, productControllers.archiveProduct);



module.exports = router;