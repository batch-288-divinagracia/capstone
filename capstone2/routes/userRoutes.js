const express = require("express");

const userControllers = require("../controllers/userControllers.js");

const auth = require("../auth.js")

const router = express.Router();

// Routes
router.post("/register", userControllers.registerUser);

router.post("/login", userControllers.loginUser);

router.get("/userInfo", auth.verify, userControllers.userInfo);

router.get("/findDetails", auth.verify, userControllers.userDetails);

router.get("/findOrders", auth.verify, userControllers.userOrders);

router.patch("/:userId", auth.verify, userControllers.updateUser);

module.exports = router;