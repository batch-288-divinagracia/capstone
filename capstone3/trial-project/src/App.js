import './css/App.css';

import {useState, useEffect, useContext} from 'react'
import {UserProvider} from './UserContext.js' 
import 'bootstrap/dist/css/bootstrap.min.css';
import AppNavBar from './components/AppNavBar.js';

//routes
import {BrowserRouter, Route, Routes} from 'react-router-dom';
import Home from './pages/Home.js';
import Register from './pages/Register.js'
import Login from './pages/Login.js'
import Logout from './pages/Logout.js'
import AdminDashBoard from './pages/AdminDashBoard.js';
import CreateProduct from './pages/CreateProduct.js'
import PageNotFound from './pages/PageNotFound.js';
import Products from './pages/Products.js';
import UpdateProduct from './pages/ProductUpdate.js';
import ActiveProducts from './pages/Store.js';
import ViewProduct from './pages/ViewProduct.js';
import ViewCart from './pages/Cart.js';




function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();
  }

 useEffect(() => {
  if(localStorage.getItem('token')){

    fetch(`${process.env.REACT_APP_API_URL}/users/userInfo`, {
      method: 'GET',
      header: {
        authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(result => result.json())
    .then(data => {
      setUser({
        id: data._id,
        isAdmin: data.isAdmin
      });
    })
  }
 }, [])

  return (

    <UserProvider value = {{user, setUser, unsetUser}}>
      <BrowserRouter>
        <AppNavBar/>
          <Routes>
           <Route path = '/' element = {<Home/>}/>
           <Route path = '/register' element = {<Register/>}/> 
           <Route path = '/login' element = {<Login/>}/>
           <Route path = '/Logout' element = {<Logout/>}/> 
           <Route path = '/products' element = {<Products/>}/>
           <Route path = '/store' element = {<ActiveProducts/>}/>
           <Route path = '/cart' element = {!user.isAdmin ? <ViewCart/> : <PageNotFound/>}/>
           <Route path = '/admin' element = {user.isAdmin ? <AdminDashBoard/> : <PageNotFound/>}/>
           <Route path = '/products/:productId/cart' element = {user.isAdmin ? <ViewProduct/> : <PageNotFound/>}/>
           <Route path = '/createProducts' element = {user.isAdmin ? <CreateProduct/> : <PageNotFound/>}/>
           <Route path = '/products/:productId' element = {user.isAdmin ? <UpdateProduct/> : <PageNotFound/>}/>
           <Route path = '*' element = {<PageNotFound/>}/>

          </Routes>

      </BrowserRouter>
    </UserProvider>
  );
}

export default App;
