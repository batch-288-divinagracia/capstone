import {Container, Nav, Navbar, Dropdown } from 'react-bootstrap';
import {Link, NavLink} from 'react-router-dom';
import {useContext} from 'react';
import UserContext from '../UserContext';


export default function AppNavBar(){

	const {user} = useContext(UserContext)

	return(

		<>

			<Navbar bg="dark" data-bs-theme="dark">
			       <Container>
			         <Navbar.Brand as = {Link} to = '/'>GameBlitz</Navbar.Brand>
			         <Nav className="ms-auto">
			           <Nav.Link as = {NavLink} to = '/'>Home</Nav.Link>
			           <Nav.Link as = {NavLink} to = '/store'>Store</Nav.Link>	
			           		    
			           {
			           		(user.isAdmin === null)?

			           		<>
			           			<Nav.Link as = {NavLink} to ='/register'>Register</Nav.Link>
			           			<Nav.Link as = {NavLink} to = '/login'>Login</Nav.Link>
			           		</>

			           		:		          			           			
		           			<>
		           			{
		           			user.isAdmin ?
		           			<>
			           		<Dropdown>
		           		      	<Dropdown.Toggle variant="dark" data-bs-theme="dark" id="dropdown-basic">
		           		        Admin
		           		      	</Dropdown.Toggle>

		           		      	<Dropdown.Menu>
		           		        	<Dropdown.Item as = {NavLink} to ='/admin'>Dash Board</Dropdown.Item>
		           		        	<Dropdown.Item as = {NavLink} to ='/products'>All Products</Dropdown.Item>
		           		        	<Dropdown.Item as = {NavLink} to ='/createProducts'>Create</Dropdown.Item>
		           		      	</Dropdown.Menu>
			           		</Dropdown>
			           		<Nav.Link as = {NavLink} to = '/logout'>Logout</Nav.Link>
			           		</>
			           		:
			           		<>
			           		<Nav.Link as = {NavLink} to = '/cart'>Cart</Nav.Link>
			           		<Nav.Link as = {NavLink} to = '/logout'>Logout</Nav.Link>
			           		</>
			           		}
			           		</>
			           }	          
			         </Nav>
			       </Container>
			     </Navbar>
		</>

		)
}