import {Button, Container, Row, Col, Carousel} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function Banner(){

	return(
		<Container>
			<Row>
				<Col className = 'mt-3 text-center'>
					<h1>GameBlitz</h1>
					<p className='fs-3 m-3'>Your one stop GameShop!</p>
				</Col>
			</Row>
			<Row>
				<Col>
					<Carousel>
					      <Carousel.Item>
					        <img
					          className="d-block w-100"
					          src="http://wallup.net/wp-content/uploads/2016/01/202400-PlayStation-video_games-logo.jpg"
					          alt="First slide"
					        />
					        <Carousel.Caption>
					          <h3>PlayStation</h3>
					          <p>Sony Entertainment (American global entertainment company)</p>
					        </Carousel.Caption>
					      </Carousel.Item>
					      <Carousel.Item>
					        <img
					          className="d-block w-100"
					          src="https://media.vandal.net/m/9-2020/20209171631897_1.jpg"
					          alt="Second slide"
					        />

					        <Carousel.Caption>
					          <h3>Final Fantasy XVI</h3>
					          <p>Upcoming 2023 action role-playing game developed and published by Square Enix</p>
					        </Carousel.Caption>
					      </Carousel.Item>
					      <Carousel.Item>
					        <img
					          className="d-block w-100"
					          src="https://external-preview.redd.it/LSNrJm6fHPO3c5Yj0ZCfnz_nPfJiQf1vvsIPY7ayPLc.png?format=pjpg&auto=webp&s=c95416a03675973545814684f9630ea83532e80d"
					          alt="Third slide"
					        />

					        <Carousel.Caption>
					          <h3>The Legend of Zelda: Tears of the Kingdom</h3>
					          <p> 2023 video game developed by Nintendo</p>
					        </Carousel.Caption>
					      </Carousel.Item>
					      <Carousel.Item>
					        <img
					          className="d-block w-100"
					          src="https://i.ytimg.com/vi/noV8vt1mpa4/maxresdefault.jpg"
					          alt="Fourth slide"
					        />
					        <Carousel.Caption>
					          <h3>Xbox</h3>
					          <p>Microsoft's video gaming brand</p>
					        </Carousel.Caption>
					      </Carousel.Item>
					    </Carousel>
				</Col>
			</Row>
		</Container>




		)
}