import {Container, Row, Col, Button, Card} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext.js';
import {Link} from 'react-router-dom';



export default function Productcard(props){

const {_id, name, description, price, stocks, isActive, createdOn} = props.productProp;


	return(
			<Container className='bg-dark'>
				<Row  className ='col-12'>
					<Col className ="m-3">
						<Card> 
		                      <Card.Body>
		                        <Card.Title>{name}</Card.Title>
		                        
		                        <Card.Subtitle>Description:</Card.Subtitle>
		                        <Card.Text>{description}</Card.Text>

		                        <Card.Subtitle>Price:</Card.Subtitle>
		                        <Card.Text>Php {price}</Card.Text>

		                        <Card.Subtitle>Stocks</Card.Subtitle>
		                        <Card.Text>{stocks}</Card.Text>

		                        <Card.Subtitle>Stocks</Card.Subtitle>
		                        <Card.Text>{isActive?"Active":"Not Active"}</Card.Text>

		                        <Card.Subtitle>Date Realeased</Card.Subtitle>
		                        <Card.Text>{createdOn}</Card.Text>
		                        <Row>
		                        	<Col>
		                        		<Button className= 'm-2'as = {Link} to = {`/products/${_id}`}>Edit</Button>
		                        	</Col>
		                        </Row>
		                        

		                      </Card.Body>
		                </Card>
					</Col>
				</Row>
			</Container>
		)
}