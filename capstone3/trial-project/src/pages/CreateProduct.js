import {Container, Row, Col, Button, Form} from 'react-bootstrap';
import {useNavigate} from 'react-router-dom'
import {useState, useContext, useEffect} from 'react';
import UserContext from '../UserContext.js';
import Swal2 from 'sweetalert2';



export default function CreateProduct(){

	const [name, setName] = useState('')
	const [description, setDescription] = useState('')
	const [price, setPrice] = useState('')
	const [stocks, setStocks] = useState('')
	const [isDisabled, setIsDisabled] = useState(true);
	const {user} = useContext(UserContext)
	const navigate = useNavigate();
	const token = localStorage.getItem('token');
	


	function clearForm(){

		setName('');
		setDescription('');
		setPrice('');
		setStocks('');

	}

	function addProduct(event){

		event.preventDefault(event);



		fetch(`${process.env.REACT_APP_API_URL}/products/addProducts`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				 authorization: `Bearer ${token}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price,
				stocks: stocks
			})
		})
		.then(result => result.json())
		.then(data => {
			if(data){
				Swal2.fire({
					title: 'Successful',
					icon: 'success',
					text: 'Product added!'
				})

				clearForm();

			} else {
				Swal2.fire({
					title: 'Unsuccessful',
					icon: 'error',
					text: "Please try again!"
				})

			}
		})

	}

	

	useEffect(() =>{
		if(name !== '' && description !== '' && price !== '' && stocks !== ''){

			setIsDisabled(false)
		} else {
			setIsDisabled(true)
		}
	}, [name, description, price, stocks])


	return(
		
		<Container className='mt-5'>
			<Row>
				<Col className='text-bg-warning'>
					<h1 className= 'm-5 text-center'>PRODUCT FORM</h1>
					<Form onSubmit = {event => addProduct(event)}>
					    <Form.Group className="mb-3" controlId="formProductName">
					      	<Form.Label>Name of Product</Form.Label>
					      	<Form.Control type="string" value = {name} onChange = {event => setName(event.target.value)} placeholder="Product Name" />
					    </Form.Group>
					    <Form.Group className="mb-3" controlId="formProductDescription">
					      	<Form.Label>Description</Form.Label>
					      	<Form.Control type="String" value = {description} onChange = {event => setDescription(event.target.value)} placeholder="Product Description" />
					    </Form.Group>
				        <Row>
				          	<Col>
				          		<Form.Group className="mb-3" controlId="formProductPrice">
					          		<Form.Label>Price</Form.Label>
					            	<Form.Control type="number" value = {price} onChange = {event => setPrice(event.target.value)} placeholder="Price" />
				            	</Form.Group>
				          	</Col>
				          	<Col>
				          		<Form.Group className="mb-3" controlId="formProductStock">
					          		<Form.Label>Stocks</Form.Label>
					            	<Form.Control type="number" value = {stocks} onChange = {event => setStocks(event.target.value)} placeholder="Stocks" />
				            	</Form.Group>
				          	</Col>
				        </Row>
						<Button className=' p-3 m-3' variant="success" type="submit"  disabled = {isDisabled}>Submit</Button>
					</Form>
				</Col>
			</Row>
		</Container>

		)
}