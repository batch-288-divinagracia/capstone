import Banner from '../components/Banner.js';
import Highlights from '../components/Highlights.js';
import {Container, Row, Col} from 'react-bootstrap'


export default function Home(){


      return(
            
            <Container className='home'>
                  <Row>
                        <Col>
                              <Banner/> 
                              <Highlights/>  
                        </Col>
                  </Row>
            </Container>
            )
}