import {Container, Row, Col, Button, Form} from 'react-bootstrap';
import {Link} from 'react-router-dom'



export default function PageNotFound(){

	
	return(
		<Container className= 'mt-5'>
			<Row>
				<Col className = "col-6 mx-auto">
					<div>
						<h1>Page Not Found</h1>
						<img className= ""src = "https://thumbs.dreamstime.com/b/liquid-error-page-not-found-design-graphic-template-website-white-background-98082973.jpg" alt="Page Not Found"/>
						<h4>Go back to the <Link to = '/'>homepage</Link>.</h4>						
					</div>
				</Col>
			</Row>
		</Container>
	)
}
