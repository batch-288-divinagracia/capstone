import Storecard from '../components/Storecard.js'
import {useState, useContext, useEffect} from 'react'
import UserContext from '../UserContext.js';
import Swal2 from 'sweetalert2'

export default function ActiveProducts(){

	const {user} =useContext(UserContext);
	const [products, setProducts] =useState('');
	const token = localStorage.getItem('token');

	useEffect(() => {
		createCart();

		fetch(`${process.env.REACT_APP_API_URL}/products/availableProducts`) 
		.then(result => result.json())
		.then(data => {
			setProducts(data.map(product => {
				return(
					<Storecard key = {product._id} storeProp = {product}/>
				)
			}))
		})
	}, [])

	function createCart(){

		fetch(`${process.env.REACT_APP_API_URL}/carts/createCart`, {
			method: 'POST',
			headers: {
				authorization: `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(data => {
			if(data){

				Swal2.fire('Welcome to GameBlitz');

			} 
		})
	}

	return(

		<>
		<h1 className = 'text-center mt-3'>GameBlitz</h1>
		{products}
		</>

		)
}